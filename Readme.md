# MQTT Decora Bluetooth Switch

This small daemon/docker image can control a Decora Bluetooth Switch:
- Leviton DDL06 V5.5

It can be use with Home-assistant

## Home-assistant configuration

```
light:
  - platform: mqtt
    name: light1
    unique_id: light1
    command_topic: "hass/light/decora/light1/switch"
    brightness_command_topic: "hass/light/decora/light1/set_brightness"
    brightness_scale: 100
    brightness_state_topic: "hass/light/decora/light1/brightness"
    # brightness_value_template:
    payload_on: "ON"
    payload_off: "OFF"
    state_topic: "hass/light/decora/light1/state"
    # state_value_template:
    availability_topic: "hass/light/decora/light1/availability"
    payload_available: "online"
    payload_not_available: "offline"

```

## Run it locally

Run it:
```
pip install -r requirements.txt

MQTT_USERNAME=hass \
	MQTT_PASSWORD=hass \
	MQTT_HOST=192.168.0.1 \
	MQTT_PORT=1883 \
	ADDR=F0:C7:7F:FF:FF:FF \
	KEY="0x4a4a4a4a" \
	ROOT_TOPIC=hass/light/decora \
	LIGHT_NAME=ligth1 \
	LOG_LEVEL=INFO \
	env/bin/python mqtt_light_decora.py
```

## Build Docker image

```
docker build -t mqttdecoralight .
```

## Run Docker container

```
docker run --rm -it \
    --name bttest \
    --privileged --net=host \
    -e MQTT_USERNAME=hass \
    -e MQTT_PASSWORD=hass \
    -e MQTT_HOST=192.168.0.1 \
    -e MQTT_PORT=1883 \
    -e ADDR=F0:C7:7F:FF:FF:FD \
    -e KEY="0x4a4a4a4a" \
    -e ROOT_TOPIC=hass/light/decora \
    -e LIGHT_NAME=light1  \
    -e LOG_LEVEL=INFO \
    mqttdecoralight:latest bash
```

# Links

* https://github.com/mjg59/python-decora
* http://ianharvey.github.io/bluepy-doc/peripheral.html
