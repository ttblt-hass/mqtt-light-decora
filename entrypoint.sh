#!/bin/bash

echo "Start Dbus"
/etc/init.d/dbus start

echo "Start Bluetooth"
bluetoothd -d &

echo "Print env"
env | sort

echo "Start daemon"
mqtt_light_decora
