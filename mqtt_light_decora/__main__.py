"""Module defining entrypoint."""
import asyncio

from mqtt_light_decora import device


def main():
    """Entrypoint function."""
    dev = device.DecoraBTDevice()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
