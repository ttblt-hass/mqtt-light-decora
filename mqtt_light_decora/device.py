"""Mqtt Decora bluetooth switch module."""
import asyncio
import json
import os

import mqtt_hass_base

from mqtt_light_decora.custom_decora import CustomDecora


class DecoraBTDevice(mqtt_hass_base.MqttDevice):
    """DecoraBTDevice class."""

    def __init__(self):
        """Constructor."""
        mqtt_hass_base.MqttDevice.__init__(self, "mqtt-decora")
        self.switch = None

    def read_config(self):
        """Read env vars."""
        self.btdev_addr = os.environ['ADDR']
        self.btdev_key = os.environ['KEY']
        try:
            self.btdev_key = int(self.btdev_key, base=16)
        except ValueError:
            try:
                self.btdev_key = int(self.btdev_key, base=10)
            except ValueError:
                raise Exception("Bad Bluetooth key")
        self.mqtt_root_topic = os.environ['ROOT_TOPIC']
        self.light_name = os.environ['LIGHT_NAME']
        self.iface = int(os.environ.get('IFACE', 0))
        self.mqtt_base_topic = (self.mqtt_root_topic.strip("/") +
                                "/light/decora/" +
                                self.light_name.strip("/"))

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on_connect callback."""

    def _on_publish(self, client, userdata, mid):
        """MQTT on_publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""
        self.mqtt_client.subscribe(self.mqtt_base_topic + "/command")
        self.mqtt_client.subscribe(self.mqtt_base_topic + "/set_brightness")

    def _on_message(self, client, userdata, msg):
        """MQTT on_message callback."""
        self.logger.debug("Message: %s %s", msg.topic, str(msg.payload))
        try:
            if msg.topic == self.mqtt_base_topic + "/command" and msg.payload == b"ON":
                self.logger.info("Set ON")
                self.switch.on()
                client.publish(topic=self.mqtt_base_topic + "/state", payload="ON")
            elif msg.topic == self.mqtt_base_topic + "/command" and msg.payload == b"OFF":
                self.logger.info("Set OFF")
                self.switch.off()
                client.publish(topic=self.mqtt_base_topic + "/state", payload="OFF")
            elif msg.topic == self.mqtt_base_topic + "/set_brightness":
                brightness = int(msg.payload)
                self.logger.info("Set brightness: %s", brightness)
                self.switch.set_brightness(brightness)

                client.publish(topic=self.mqtt_base_topic + "/brightness", payload=str(brightness))
        except BaseException as exp:
            self.logger.error(exp)

    def _signal_handler(self, signal_, frame):
        """Signal handler."""

    async def _init_main_loop(self):
        """Init before starting main loop."""
        self._bt_connect()
        self._send_mqtt_config()

    def _send_mqtt_config(self):
        """Send MQTT config to Home Assistant."""
        mqtt_config_topic = self.mqtt_base_topic + "/config"
        switch_config = {"name": self.light_name,
                         "command_topic": self.mqtt_base_topic + "/command",
                         "brightness_command_topic": self.mqtt_base_topic + "/set_brightness",
                         "brightness_scale": 100,
                         "brightness_state_topic": self.mqtt_base_topic + "/brightness",
                         "state_topic": self.mqtt_base_topic + "/state",
                         "payload_on": "ON",
                         "payload_off": "OFF",
                         "availability_topic": self.mqtt_base_topic + "/availability",
                         "payload_available": "online",
                         "payload_not_available": "offline",
                         "qos": 0,
                         "optimistic": False,
                         "device": {"identifiers": self.switch.mac,
                                    "manufacturer": "leviton",
                                    }
                         }
        self.logger.info("New switch config: %s, %s", mqtt_config_topic, switch_config)
        self.mqtt_client.publish(topic=mqtt_config_topic,
                                 retain=True,
                                 payload=json.dumps(switch_config))

    def _bt_connect(self):
        """Connect to the bluetooth device."""
        self.logger.info("Connecting to bluetooth device")
        self.switch = CustomDecora(self.btdev_addr, self.btdev_key, self.iface)
        self.switch.connect()
        self.logger.info("Connected to bluetooth device")

    async def _main_loop(self):
        # Get data from the BT device
        try:
            self.switch.update_state()
            self.mqtt_client.publish(topic=self.mqtt_base_topic + "/availability",
                                     payload="online")
        except BaseException:
            self.logger.error("Can not update light state")
            self.mqtt_client.publish(topic=self.mqtt_base_topic + "/availability",
                                     payload="offline")
            self.logger.error("Try to connect to the light")
            self._bt_connect()
            return
        # Send brightness
        brightness = self.switch.level
        self.logger.debug("brigthness: %s", brightness)
        self.mqtt_client.publish(topic=self.mqtt_base_topic + "/brightness", payload=brightness)

        # Send state
        state = self.switch.power
        if state == 1:
            self.mqtt_client.publish(topic=self.mqtt_base_topic + "/state", payload="ON")
        else:
            self.mqtt_client.publish(topic=self.mqtt_base_topic + "/state", payload="OFF")

        # wait
        self.main_loop_wait_time = 5
        i = 0
        while i < self.main_loop_wait_time and self.must_run:
            await asyncio.sleep(1)
            i += 1

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
        mqtt_config_topic = self.mqtt_base_topic + "/config"
        self.logger.info("Remove switch config: %s", mqtt_config_topic)
        self.mqtt_client.publish(topic=mqtt_config_topic,
                                 retain=True,
                                 payload=json.dumps({}))
